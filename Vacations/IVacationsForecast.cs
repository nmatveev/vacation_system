using System;

namespace Vacations
{
    public interface IVacationsForecast
    {
        DateTime Date { get; set; }
    }
}