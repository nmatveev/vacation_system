﻿using System;
using System.Collections.Generic;

namespace VacationsCore
{
    public interface IUser
    {
        Guid Guid { get; set; }
        List<string> Login { get; set; }
    }
}